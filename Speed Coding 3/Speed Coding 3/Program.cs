﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speed_Coding_3
{
    class Program
    {
        #region Variables
        static int[] m_zahlen = new int[] {5, -7, 7, 3, -2, 5, 4, -4, 4 };
        static int m_check;
        static int remember;
        static int m_firstCounter = 0;
        #endregion

        static void Main(string[] args)
        {
            // First takes a number from the array and remember it.
            for (m_firstCounter = 0; m_firstCounter < m_zahlen.Length; m_firstCounter++)
            {
                m_check = m_zahlen[m_firstCounter];

                // Check through the array if there is a number bigger then the to check number.
                for (int j = 0; j < m_zahlen.Length; j++)
                {
                    // First only looks at the positive version of the number.
                    if (Math.Abs(remember) <= Math.Abs(m_zahlen[j]))
                    {
                        // Here it checks if the number, if there is one which is the same but has - or + diffrence.
                        // If the remembered biggest number is smaller than the one in the array it will save the array number into the variable.
                        if (remember <= m_zahlen[j])
                        {
                            remember = m_zahlen[j];
                        }
                        continue;
                    }
                }
            }
            Console.WriteLine(remember);
            Console.ReadLine();
        }
    }
}
