﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Speed_Coding
{
    class Program
    {
        private static string input;

        private static char toCheck;

        private static int count = 0;

        static void Main(string[] args)
        {
            Console.WriteLine("Please write a word! \nThe first NOT double letter will be thrown out in the console.");
            input = Console.ReadLine().ToUpper();

            // In this for one letter will be saved from the string. From the first to last.
            for (int j = 0; j < input.Length; j++)
            {
                // Reset the counter.
                count = 0;
                // Save one letter from the string.
                toCheck = input[j];

                // In this for the saved letter will be checked with every letter in the string.
                for (int i = 0; i < input.Length; i++)
                {
                    // If the counters are not the same this will happen otherwise it will continue.
                    // If the counter are the same it means that it is the same letter.
                    if (j != i)
                    {
                        // Checks if the saved letter is the same or not. If yes the counter will increase otherwise nothing will happen.
                        if (toCheck == input[i])
                        {
                            count++;
                        }
                    }
                    else
                    {
                        continue;
                    }

                    // If the counter i reached the end and the counter is still 0 this will happen.
                    // Otherwise nothing will happen and the next letter will be checked.
                    // input.Length - 1 because otherwise input.Length will be bigger then i.
                    if (i == input.Length - 1 && count == 0)
                    {
                        // Throws out the first letter which is not double in the word.
                        Console.WriteLine($"The first NOT double letter is: '{toCheck}'.");
                        Console.ReadKey();
                        return;
                    }
                }
            }

            // Only will be thrown out if there is no unique letter.
            Console.WriteLine($"No unique letters.");
            Console.ReadKey();
        }
    }
}
