﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody m_rigidbody;
    private Camera m_main;

    [SerializeField]
    float m_moveSpeed = 0f;
    [SerializeField]
    float m_rotateSpeed = 0f;

    // Start is called before the first frame update
    void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_main = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Rotation();
        Movement();
    }

    void Rotation()
    {
        m_main.transform.localEulerAngles += new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * m_rotateSpeed * Time.deltaTime;
    }

    void Movement()
    {
        Vector3 dir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        dir = dir.normalized;

        if (dir != new Vector3(0, 0, 0))
        {
            transform.localEulerAngles = m_main.transform.localEulerAngles;
            m_rigidbody.velocity = dir * m_moveSpeed * Time.deltaTime;
        }
    }
}
