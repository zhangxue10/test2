﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    GameObject[] AllObjects;
    GameObject Player;
    Rigidbody playerBody;
    float movementSpeed = 200f;

    // Start is called before the first frame update
    void Start()
    {
        AllObjects = FindObjectsOfType<GameObject>();

        foreach (GameObject _object in AllObjects)
        {
            if (_object.name == "TheCube")
            {
                Player = _object;
            }
        }

        Player.AddComponent<Rigidbody>();
        playerBody = Player.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
    }

    public void Move()
    {
        Vector3 dir = new Vector3(0, Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        dir = dir.normalized;

        playerBody.velocity = new Vector3(0, dir.y * movementSpeed, dir.z * movementSpeed);
    }

    public void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            playerBody.AddForce(Vector3.up * 100, ForceMode.Impulse);
        }
    }
}
