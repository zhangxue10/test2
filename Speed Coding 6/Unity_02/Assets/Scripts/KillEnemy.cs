﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillEnemy : MonoBehaviour
{
    List<int> toKill = new List<int>(3);

    private int access = 0;

    private int test = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //// Funkt aber zu spät.

        //Enemy[] enemies = FindObjectsOfType<Enemy>();
        //for (int i = 0; i < enemies.Length; i++)
        //{
        //    if (enemies[i].CurrentHP == 0)
        //    {
        //        if (AccessController.Instance.CanAccess())
        //        {
        //            Destroy(enemies[i].gameObject);
        //        }
        //    }
        //}

        // Auch zu spät. Und hat etwas mit Zufall zu tun.
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        for (int i = 0; i < enemies.Length; i++)
        {
            access++;
            test++;
            if (test >= enemies.Length)
            {
                test = 0;
            }

            if (enemies[test].CurrentHP <= 0)
            {
                toKill.Add(test);
            }

            if (access >= 3)
            {
                i = enemies.Length;
            }
        }

        foreach (int toKill in toKill)
        {
            Destroy(enemies[toKill].gameObject);
        }
        toKill.Clear();

        // Sonstige Idee für alle enemies die depletion rate rausrechnen
        // und dann der mit dem höchsten zur niedrigsten in einer Liste oder Array speichern.
        // Und dann von höchsten depletion rate zum niedrigsten nur die ersten drei abfragen.
        // Und je nachdem zerstören oder lassen und dann die nächsten drei.
        // Liste ist dabei einfach da die enemies dort rausgelöscht werden können nach dem zerstören.
    }
}
